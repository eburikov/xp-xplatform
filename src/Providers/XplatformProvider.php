<?php

namespace Xplatform\Xplatform\Providers;

use Illuminate\Support\ServiceProvider;
use Xplatform\KafkaService\KafkaService;
use Xplatform\KafkaService\Console\Commands\KafkaQueue;

class XplatformProvider extends ServiceProvider
{
    protected $handlers = [];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
