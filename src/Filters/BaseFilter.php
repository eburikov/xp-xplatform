<?php
namespace Xplatform\Xplatform\Filters;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Xplatform\Xplatform\Helpers\Helpers;

abstract class BaseFilter {
    protected string $name;

    protected string $field;

    protected string|null $relatedField;

    protected bool $multiple = false;

    protected Closure|null $customQuery = null;

    protected function __construct(
        string $name,
        string $field,
        string|null $relatedField = null
    ) {
        $this->setName($name);
        $this->setField($field);
        $this->setRelatedField($relatedField);
    }

    public static function make (...$arguments): static
    {
        return new static(...$arguments);
    }

    public function setName (string $name): static {
        $this->name = $name;
        return $this;
    }

    public function setField (string $field): static {
        $this->field = $field;
        return $this;
    }

    public function setRelatedField (string|null $relatedField): static {
        $this->relatedField = $relatedField;
        return $this;
    }

    public function setCustomQuery (Closure|null $customQuery): static {
        $this->customQuery = $customQuery;
        return $this;
    }

    public function getCustomQuery (): Closure|null {
        return $this->customQuery;
    }

    public function getField (): string {
        return $this->field;
    }

    public function getRelatedField (): string {
        return $this->relatedField;
    }

    protected function requestValue (): string|array|null {
        $filter = json_decode(request()->filter);

        if ($filter) {
            if (property_exists($filter, $this->name)) {
                return is_array($filter->{$this->name})
                    ? Helpers::normalizeObjectsArrayByKey($filter->{$this->name})
                    : Helpers::normalizeObjectByKey($filter->{$this->name});
            }
        }

        return null;
    }

    public function handle (Builder $query): Builder
    {
        if ($this->requestValue()) {
            if ($this->customQuery) {
                $query = $query->where($this->getCustomQuery());
            } elseif ($this->relatedField) {
                $query = $query->whereHas($this->relatedField, function ($q) {
                    if (is_array($this->requestValue())) {
                        $q->whereIn($this->getField(), $this->requestValue());
                    } else {
                        $q->where($this->getField(), $this->requestValue());
                    }
                });
            } else {
                if (is_array($this->requestValue())) {
                    $query = $query->whereIn($this->getField(), $this->requestValue());
                } else {
                    $query = $query->where($this->getField(), $this->requestValue());
                }
            }
        }
        return $query;
    }
}
