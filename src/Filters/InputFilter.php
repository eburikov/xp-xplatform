<?php
namespace Xplatform\Xplatform\Filters;

use Illuminate\Database\Eloquent\Builder;

class InputFilter extends BaseFilter {
    protected $partialMatch = false;

    public function partialMatch(): static {
        $this->partialMatch = true;
        return $this;
    }

    public function handle (Builder $query): Builder {
        if ($this->requestValue()) {
            if ($this->partialMatch) {
                $query = $query->where($this->field, 'ilike' , '%'.$this->requestValue().'%');
            } else {
                $query = $query->where(
                    \DB::raw('lower('.$this->field.')'),
                    \DB::raw('lower(\''.strtolower($this->requestValue()).'\')')
                );
            }
        }
        return $query;
    }
}
