<?php
namespace Xplatform\Xplatform\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

use Xplatform\Xplatform\Traits\CamelCasing;
use Xplatform\Xplatform\Traits\HasLoadableRelations;
use Xplatform\Xplatform\Traits\Sortable;
use Xplatform\Xplatform\Traits\Partitionable;

class Model extends EloquentModel {
  use CamelCasing, HasLoadableRelations, Sortable, Partitionable;
}