<?php

namespace Xplatform\Xplatform\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Xplatform\Xplatform\Traits\HasAlias;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    protected $casts = [
        'values' => 'object',
        'is_hidden' => 'boolean',
        'is_mandatory' => 'boolean',
    ];
}
