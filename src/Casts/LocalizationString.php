<?php

namespace Xplatform\Xplatform\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class LocalizationString implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return array
     */
    public function get($model, $key, $value, $attributes)
    {
        $value = json_decode($value, true);
        $locale = app()->getLocale();
        return $value && array_key_exists($locale, $value) ? $value[$locale] : null;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  array  $value
     * @param  array  $attributes
     * @return string
     */
    public function set($model, $key, $value, $attributes)
    {
        return json_encode($value);
    }
}
