<?php

namespace Xplatform\Xplatform\Middleware;

use Closure;
use Illuminate\Http\Request;
use ReallySimpleJWT\Token;

class CheckJwtToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$jwt = $request->header('X-API-Key')) {
            return abort(401);
        }

        if (!Token::validate($jwt, env('JWT_SECRET'))) {
            return abort(403);
        }

        return $next($request);
    }
}
