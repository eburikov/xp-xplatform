<?php
namespace Xplatform\Xplatform\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Filterable {
    public function scopeFiltered (Builder $query): Builder {
        foreach($this->filters() as $filter) {
            $query = $filter->handle($query);
        }

        return $query;
    }

    abstract public function filters (): array;
}
