<?php
namespace Xplatform\Xplatform\Traits;

use Illuminate\Http\Request;

trait HasAttributes {
  public static function createAttributes (Request $request, $item) {
    if($request->filled('attrs')) {
      collect($request->attrs)->each(function($attr) use ($item) {
        $item->attrs()->create([
          'alias' => self::generateAlias($attr['title']),
          'title' => $attr['title'],
          'type' => $attr['type'],
          'is_mandatory' => array_key_exists('isMandatory', $attr) ? $attr['isMandatory'] : false,
          'is_hidden' => array_key_exists('isHidden', $attr) ? $attr['isHidden'] : false,
          'default_value' => array_key_exists('defaultValue', $attr) ? $attr['defaultValue'] : null,
          'values' => array_key_exists('values', $attr) ? $attr['values'] : null
        ]);
      });
    }
  }

  public static function updateAttributes (Request $request, $item) {
    if($request->filled('attrs')) {
      collect($request->attrs)->each(function($attr) use ($item) {
        if (array_key_exists('id', $attr)) {
          $item->attrs()->find($attr['id'])->update([
            'title' => $attr['title'],
            'type' => $attr['type'],
            'is_mandatory' => array_key_exists('isMandatory', $attr) ? $attr['isMandatory'] : false,
            'is_hidden' => array_key_exists('isHidden', $attr) ? $attr['isHidden'] : false,
            'default_value' => array_key_exists('defaultValue', $attr) ? $attr['defaultValue'] : null,
            'values' => array_key_exists('values', $attr) ? $attr['values'] : null
          ]);
        } else {
          $item->attrs()->create([
            'alias' => self::generateAlias($attr['title']),
            'title' => $attr['title'],
            'type' => $attr['type'],
            'is_mandatory' => array_key_exists('isMandatory', $attr) ? $attr['isMandatory'] : false,
            'is_hidden' => array_key_exists('isHidden', $attr) ? $attr['isHidden'] : false,
            'default_value' => array_key_exists('defaultValue', $attr) ? $attr['defaultValue'] : null,
            'values' => array_key_exists('values', $attr) ? $attr['values'] : null
          ]);
        }
      });
    }
  }
}