<?php

namespace Xplatform\Xplatform\Traits;

trait Publishable {
    public static function boot() {
        parent::boot();

        static::created(function ($item) {
            (self::$storeEvent)::dispatch($item->fresh());
        });

        static::updated(function ($item) {
            (self::$storeEvent)::dispatch($item->fresh());
        });

        static::deleted(function ($item) {
            (self::$deleteEvent)::dispatch($item);
        });
    }

    public static function publishAll () {
        self::all()->each(function ($item) {
            (self::$storeEvent)::dispatch($item);
        });
    }
}
