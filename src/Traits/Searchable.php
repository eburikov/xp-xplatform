<?php
namespace Xplatform\Xplatform\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Searchable {
    public function scopeSearched (Builder $query): Builder {
        $search = request()->search;

        if ($search) {
            $query = $query->where(function ($q) use ($search) {
                foreach ($this->searchers() as $searchField) {
                    if (is_string($searchField)) {
                        $q->orWhere($searchField, 'ilike', '%'.$search.'%');
                    } else {
                        $q->orWhere($searchField);
                    }
                }
            });
        }

        return $query;
    }

    abstract public function searchers (): array;
}
