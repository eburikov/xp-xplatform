<?php

namespace Xplatform\Xplatform\Traits;

trait HasAlias {
  public static function getByAlias ($alias) {
    return self::where('alias', $alias)->first();
  }

  public static function generateAlias ($str) {
    $converter = array(
      'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
      'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
      'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
      'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
      'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
      'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
      'э' => 'e',    'ю' => 'yu',   'я' => 'ya',
    );
   
    $str = mb_strtolower($str);
    $str = strtr($str, $converter);
    $str = mb_ereg_replace('[^-0-9a-z]', '_', $str);
    $str = mb_ereg_replace('[-]+', '_', $str);
    $str = trim($str, '_');	
   
    return $str;
  }
}