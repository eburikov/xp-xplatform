<?php
namespace Xplatform\Xplatform\Traits;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;

trait HasLoadableRelations {
  public function scopeLoaded (Builder $query): Builder {
    $load = request()->load;

    if ($load) {
      Str::of($load)->split('/[\s,]+/')->each(
        function ($relation) use (&$query) {
          $query = $query->with(str_replace(';', ',', $relation));
        }
      );
    }

    return $query;
  }
}