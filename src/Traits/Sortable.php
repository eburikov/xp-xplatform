<?php
namespace Xplatform\Xplatform\Traits;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;

trait Sortable {
  public function scopeSorted(Builder $query): Builder {
    $sort = request()->sort;

    if ($sort) {
      Str::of($sort.',id')->split('/[\s,]+/')->each(function ($item, $index) use (&$query) {
        preg_match('/(-?)(\S+)/', Str::of(Str::snake($item)), $sort);
        $query = $sort[1] === '-'
          ? $query->orderBy($sort[2], 'desc')
          : $query->orderBy($sort[2]);
      });
    }

    return $query;
  }
}