<?php
namespace Xplatform\Xplatform\Traits;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\CursorPaginator;
use Illuminate\Pagination\LengthAwarePaginator;

trait Partitionable {
  public function scopePartitioned(Builder $query): Collection|CursorPaginator|LengthAwarePaginator {
    if (request()->paginate) {
      $query = $query->paginate(request()->paginate);
    } elseif (request()->cursor) {
      $query = $query->cursorPaginate(25);
    } else {
      $query = $query->get();
    }

    return $query;
  }
}