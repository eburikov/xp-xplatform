<?php

namespace Xplatform\Xplatform\Helpers;

class Helpers {
  public static function normalizeArrayByKey(Array $array, String $key = 'id'): Array {
    return array_map(function ($item) use ($key) {
      return is_array($item)
        ? (array_key_exists($key, $item) ? $item[$key] : null)
        : $item;
      }, $array);
  }

  public static function normalizeObjectsArrayByKey(Array $array, String $key = 'id'): Array {
    return array_map(function ($item) use ($key) {
      return is_object($item)
        ? (property_exists($item, $key) ? $item->{$key} : null)
        : $item;
      }, $array);
  }

  public static function normalizeObjectByKey($item, String $key = 'id') {
    return is_object($item)
      ? (property_exists($item, $key) ? $item->{$key} : null)
      : $item;
  }
}